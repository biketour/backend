package com.tim.webengineering.biketour.repositories;

import com.tim.webengineering.biketour.models.Leg;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LegRepository extends JpaRepository<Leg, Long> {
}
