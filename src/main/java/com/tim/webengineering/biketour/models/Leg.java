package com.tim.webengineering.biketour.models;

import javax.persistence.*;

@Entity
@Table(name="legs")
public class Leg {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long id;

    private String description;

    private double latitude;

    private double longitude;

    private int number;

    @Column(name="overnight_stay")
    private String overnightStay;

    @Column(name="support_vehicle")
    private String supportVehicle;

    private String driver;

    public Leg() {}

    public Leg(String description, double latitude, double longitude, int number, String overnightStay, String supportVehicle, String driver) {
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.number = number;
        this.overnightStay = overnightStay;
        this.supportVehicle = supportVehicle;
        this.driver = driver;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOvernightStay() {
        return overnightStay;
    }

    public void setOvernightStay(String overnightStay) {
        this.overnightStay = overnightStay;
    }

    public String getSupportVehicle() {
        return supportVehicle;
    }

    public void setSupportVehicle(String supportVehicle) {
        this.supportVehicle = supportVehicle;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
}
