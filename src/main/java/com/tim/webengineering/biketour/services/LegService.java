package com.tim.webengineering.biketour.services;

import com.tim.webengineering.biketour.exceptions.NotFoundException;
import com.tim.webengineering.biketour.models.Leg;
import com.tim.webengineering.biketour.repositories.LegRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LegService implements ILegService {

    @Autowired
    private LegRepository legRepository;

    @Override
    public List<Leg> getAll() {
        return legRepository.findAll();
    }

    @Override
    public Leg getLegById(Long id) throws NotFoundException {
        Optional<Leg> leg = legRepository.findById(id);
        if (leg.isPresent()) {
            return leg.get();
        }
        throw new NotFoundException(String.format("No Leg found with id: %d", id));
    }

    @Override
    public void deleteById(Long id) throws NotFoundException {
        if (legRepository.existsById(id)) {
            legRepository.deleteById(id);
        } else {
            throw new NotFoundException(String.format("No Leg found with id: %d", id));
        }
    }

    @Override
    public Leg create(Leg leg) {
        return legRepository.save(leg);
    }

    @Override
    public Leg update(Leg leg) {
        return legRepository.save(leg);
    }
}
