package com.tim.webengineering.biketour.services;

import com.tim.webengineering.biketour.exceptions.NotFoundException;
import com.tim.webengineering.biketour.models.Leg;

import java.util.List;

public interface ILegService {

    List<Leg> getAll();
    Leg getLegById(Long id) throws NotFoundException;
    void deleteById(Long id) throws NotFoundException;
    Leg create(Leg leg);
    Leg update(Leg leg);

}
