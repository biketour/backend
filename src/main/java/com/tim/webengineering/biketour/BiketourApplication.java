package com.tim.webengineering.biketour;

import com.tim.webengineering.biketour.models.Leg;
import com.tim.webengineering.biketour.repositories.LegRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class BiketourApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiketourApplication.class, args);
	}

	@Bean
	public CommandLineRunner run(LegRepository repository) {
		return  (args -> {
			Leg leg1 = new Leg("Etappe 1", 42.0000, 11.0000, 1, "Hütte auf dem Berg", "Heli", "Klaus");
			Leg leg2 = new Leg("Etappe 2", 42.5700, 12.0000, 2, "Zelt am See", "Bus", "Max");
			Leg leg3 = new Leg("Etappe 3", 43.1000, 11.7000, 3, "Unter der Brücke", "Boot", "Susanne");

			repository.save(leg1);
			repository.save(leg2);
			repository.save(leg3);
		});
	}

}
