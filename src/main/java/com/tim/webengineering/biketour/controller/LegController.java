package com.tim.webengineering.biketour.controller;

import com.tim.webengineering.biketour.exceptions.NotFoundException;
import com.tim.webengineering.biketour.models.Leg;
import com.tim.webengineering.biketour.services.ILegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/legs")
public class LegController {

    @Autowired
    private ILegService legService;

    @GetMapping
    public List<Leg> getAll() {
        return legService.getAll();
    }

    @GetMapping(value="{id}")
    public Leg getByID(@PathVariable("id") Long id) throws NotFoundException {
        return legService.getLegById(id);
    }

    @DeleteMapping(value="{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("id") Long id) throws NotFoundException {
        legService.deleteById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public long create(@RequestBody Leg leg) {
        Leg newLeg = legService.create(leg);
        return newLeg.getId();
    }

    @PutMapping
    public Leg update(@RequestBody Leg leg) {
        return legService.update(leg);
    }

}
